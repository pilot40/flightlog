﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace flightlog
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            LoadData();
        }



        private void LoadData()
        {
            // строка подключения к БД
            //string connStr = "server=pilot208.ru;user=mister_zuev;database=mister_test;password=tms244b;";
            string connStr = "server=192.168.0.1;user=root;database=mihazmg;password=tms244b;";
            // создаём объект для подключения к БД
            MySqlConnection conn = new MySqlConnection(connStr);
            // устанавливаем соединение с БД

            //////////////////////////////////
            conn.Open();

            string queryapfrom = "SELECT airports FROM airports ORDER BY id";

            MySqlCommand commandapfrom = new MySqlCommand(queryapfrom, conn);

            MySqlDataReader readerapfrom = commandapfrom.ExecuteReader();
            //MySqlDataReader mysql_rd = conn;
            try
            {
                while (readerapfrom.Read())
                {
                    cbAirportsFrom.Items.Add(readerapfrom.GetString(0));
                    cbAirportsTo.Items.Add(readerapfrom.GetString(0));
                }
            }
            catch { }
            conn.Close();
            /////////////////////////////////////////////////////////////
            conn.Open();

            string querytime = "SELECT time FROM time ORDER BY id";

            MySqlCommand commandtime = new MySqlCommand(querytime, conn);

            MySqlDataReader readertime = commandtime.ExecuteReader();
            //MySqlDataReader mysql_rd = conn;
            try
            {
                while (readertime.Read())
                {
                    cbTime.Items.Add(readertime.GetString(0));
                    cbNight.Items.Add(readertime.GetString(0));
                }
            }
            catch { }
            conn.Close();

            /////////////////////////////////////////////////////////////
            conn.Open();
            string querypilot = "SELECT pilots FROM pilots ORDER BY id";

            MySqlCommand commandpilot = new MySqlCommand(querypilot, conn);

            MySqlDataReader readerpilot = commandpilot.ExecuteReader();

            try
            {
                while (readerpilot.Read())
                {
                    cbPilot.Items.Add(readerpilot.GetString(0));
                }
            }

            catch { }

            // закрываем соединение с БД
            conn.Close();
        }

        }
 
}
