﻿namespace flightlog
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbAirportsFrom = new System.Windows.Forms.ComboBox();
            this.cbPilot = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cbAirportsTo = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tbNzad = new System.Windows.Forms.TextBox();
            this.cbTime = new System.Windows.Forms.ComboBox();
            this.cbNight = new System.Windows.Forms.ComboBox();
            this.dtp = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(645, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Пилот:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(145, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Откуда:";
            // 
            // cbAirportsFrom
            // 
            this.cbAirportsFrom.FormattingEnabled = true;
            this.cbAirportsFrom.Location = new System.Drawing.Point(148, 39);
            this.cbAirportsFrom.Name = "cbAirportsFrom";
            this.cbAirportsFrom.Size = new System.Drawing.Size(131, 21);
            this.cbAirportsFrom.TabIndex = 4;
            // 
            // cbPilot
            // 
            this.cbPilot.FormattingEnabled = true;
            this.cbPilot.Location = new System.Drawing.Point(620, 39);
            this.cbPilot.Name = "cbPilot";
            this.cbPilot.Size = new System.Drawing.Size(131, 21);
            this.cbPilot.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Дата:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(434, 13);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Время:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(533, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(40, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Ночь:";
            // 
            // cbAirportsTo
            // 
            this.cbAirportsTo.FormattingEnabled = true;
            this.cbAirportsTo.Location = new System.Drawing.Point(294, 39);
            this.cbAirportsTo.Name = "cbAirportsTo";
            this.cbAirportsTo.Size = new System.Drawing.Size(131, 21);
            this.cbAirportsTo.TabIndex = 9;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(291, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(39, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Куда:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(763, 13);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 11;
            this.label7.Text = "№ Задания";
            // 
            // tbNzad
            // 
            this.tbNzad.Location = new System.Drawing.Point(766, 38);
            this.tbNzad.Name = "tbNzad";
            this.tbNzad.Size = new System.Drawing.Size(100, 20);
            this.tbNzad.TabIndex = 12;
            // 
            // cbTime
            // 
            this.cbTime.FormattingEnabled = true;
            this.cbTime.Location = new System.Drawing.Point(437, 38);
            this.cbTime.Name = "cbTime";
            this.cbTime.Size = new System.Drawing.Size(72, 21);
            this.cbTime.TabIndex = 13;
            // 
            // cbNight
            // 
            this.cbNight.FormattingEnabled = true;
            this.cbNight.Location = new System.Drawing.Point(536, 39);
            this.cbNight.Name = "cbNight";
            this.cbNight.Size = new System.Drawing.Size(72, 21);
            this.cbNight.TabIndex = 14;
            // 
            // dtp
            // 
            this.dtp.CustomFormat = "dd.MM.yyyy";
            this.dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtp.ImeMode = System.Windows.Forms.ImeMode.On;
            this.dtp.Location = new System.Drawing.Point(15, 39);
            this.dtp.Name = "dtp";
            this.dtp.Size = new System.Drawing.Size(107, 20);
            this.dtp.TabIndex = 15;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(885, 398);
            this.Controls.Add(this.dtp);
            this.Controls.Add(this.cbNight);
            this.Controls.Add(this.cbTime);
            this.Controls.Add(this.tbNzad);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cbAirportsTo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cbPilot);
            this.Controls.Add(this.cbAirportsFrom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Лётная книжка";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cbAirportsFrom;
        private System.Windows.Forms.ComboBox cbPilot;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cbAirportsTo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox tbNzad;
        private System.Windows.Forms.ComboBox cbTime;
        private System.Windows.Forms.ComboBox cbNight;
        private System.Windows.Forms.DateTimePicker dtp;
    }
}

